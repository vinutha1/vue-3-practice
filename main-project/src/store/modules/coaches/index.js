import mutations from './mutations.js';
import actions from './actions.js';
import getters from './getters.js';

export default {
  namespaced: true,
  state() {
    return {
      lastFetch: null,
      coaches: [
        {
          id: 'c1098',
          firstName: 'Chendan',
          lastName: 'Rohit',
          areas: ['frontend', 'career'],
          description:
            "I'm Chendan ,I am a software professional working for 10years",
          hourlyRate: 300
        },
        {
          id: 'c0782',
          firstName: 'John',
          lastName: 'RO',
          areas: ['frontend', 'career', 'backend'],
          description:
            'I am John and as a senior developer in a big tech company.',
          hourlyRate: 30
        }
      ]
    };
  },
  mutations,
  actions,
  getters
};
