const app = Vue.createApp({
	data() {
		return {
			newTask: "",
			tasks: [],
			listVisible: true
		};
	},
	methods: {
		addTask() {
			if (this.newTask != "") {
				this.tasks.push(this.newTask);
			}
		}
	}
});
app.mount("#assignment");
