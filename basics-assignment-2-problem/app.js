const app = Vue.createApp({
	data() {
		return {
			text: "",
			confirmedText: "",
			popUpText: "Vue 3 course"
		};
	},
	methods: {
		showAlert() {
			alert(this.popUpText);
		},
		userInput(event) {
			this.text = event.target.value;
		},
		showOnEnter(event) {
			this.confirmedText = event.target.value;
		}
	}
});
app.mount("#assignment");
