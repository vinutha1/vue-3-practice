const app = Vue.createApp({
	data() {
		return { num: 0 };
	},
	methods: {
		add(digit) {
			this.num = this.num + digit;
		}
	},
	computed: {
		result() {
			if (this.num < 37) {
				return "Not there yet";
			} else if (this.num == 37) {
				return 37;
			} else {
				return "Too much!!";
			}
		}
	},
	watch: {
		result() {
			let self = this;
			setTimeout(function () {
				self.num = 0;
			}, 5000);
		}
	}
});
app.mount("#assignment");
